package com.scale360
package workshop.di

import org.scalatest._
import org.scalatest.mockito._

abstract class UnitTest extends FlatSpec
  with Matchers
  with EitherValues
  with Inside
  with MockitoSugar
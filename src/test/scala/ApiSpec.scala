package com.scale360
package workshop.di

import org.http4s._
import cats.effect.IO

class ApiSpec extends UnitTest {
  behavior of "API"

  it should "get list of order items" in {
    val method = Method.GET
    val uri = Uri unsafeFromString "/items"
    val request = Request[IO](method, uri)
    val responseOpt = Api.routes.run(request).value.unsafeRunSync()
    responseOpt shouldBe defined
    val response = responseOpt.get
    response.status shouldBe Status.Ok
    val responseBody = response.as[String].unsafeRunSync()
    responseBody should not be empty
  }
  it should "get list of receipts" is pending
  it should "get detail of any single receipt" is pending
  it should "create new receipt" is pending
  it should "add orders to existing receipt" is pending
  it should "print receipt" is pending
  it should "close receipt" is pending
}
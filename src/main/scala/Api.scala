package com.scale360
package workshop.di

import java.time.Instant
import org.http4s._
import org.http4s.dsl.io._
import cats.effect.IO

import RequestBodyCodec._


object ReceiptIdVar {
  def unapply(s: String): Option[Receipt.Id] = LongVar.unapply(s).map(Receipt.Id.apply)
}

object Api {
  def routes = HttpService[IO]{
    case GET -> Root / "items" => getAllItems
    case GET -> Root / "receipts" => getAllReceipts
    case GET -> Root / "receipts" / ReceiptIdVar(id) => getReceipt(id)
    case POST -> Root / "receipts" => createReceipt
    case req @ POST -> Root / "receipts" / ReceiptIdVar(id) / "order" =>
      for {
        data <- req.as[Seq[OrderData]]
        response <- addOrders(id, data)
      } yield response
    case POST -> Root / "receipts" / ReceiptIdVar(id) / "print" => printReceipt(id)
    case req @ POST -> Root / "receipts" / ReceiptIdVar(id) / "close" => for {
      data <- req.as[CloseReceiptData]
      response <- closeReceipt(id, data)
    } yield response
  }

  private def getAllItems = Ok(
    ItemList.all.map(_.toString).mkString("\n")
  )

  private def getAllReceipts = Ok(
    ReceiptRepository.readAll().map(_.toString).mkString("\n")
  )

  private def getReceipt(id: Receipt.Id) = {
    val result = ReceiptRepository.read(id)
    result match {
      case Right(r) =>
        Ok(r.toString)
      case Left(err) => BadRequest(err.toString)
    }
  }

  private def createReceipt = {
    val time = Instant.now()
    val id = ReceiptRepository.create(CreateReceiptData(Receipt.OpenTime(time)))
    Ok(id.toString)
  }

  private def addOrders(id: Receipt.Id, data: Seq[OrderData]) = {
    val allItems = ItemList.all
    val inputItems = for {
      OrderData(id, amt) <- data
      item <- allItems.find(_.id == id)
    } yield item -> amt
    val result = for {
      _ <- checkItemIds(data.map(_.itemId).toSet, allItems.map(_.id).toSet)
      _ <- ReceiptRepository.update(id)(_ addItemMap inputItems.toMap).left.map(_.toString)
    } yield ()
    result match {
      case Right(_) => Ok()
      case Left(err) => BadRequest(err)
    }
  }

  private def checkItemIds(inputIds: Set[Item.Id], existingIds: Set[Item.Id]) =
    if(inputIds subsetOf existingIds) Right(())
    else Left("input contain invalid ids")

  private def printReceipt(id: Receipt.Id) = {
    val result = ReceiptRepository.read(id)
    result match {
      case Right(receipt) =>
        ReceiptPrinter.print(receipt, None)
        Ok()
      case Left(err) => BadRequest(err.toString)
    }
  }

  private def closeReceipt(id: Receipt.Id, data: CloseReceiptData) = {
    val time = Instant.now()
    val setStatusClosed = (_: Receipt) setStatus Receipt.Status.Closed(Receipt.CloseTime(time))
    val result = for {
      receipt <- ReceiptRepository.read(id).left.map(formatRepositoryError)
      _ <- checkStatusForClosing(receipt)
      change <- calculateChange(data.paidAmount, receipt.calculateGrossTotal)
      _ <- ReceiptRepository.update(id)(setStatusClosed)
        .left.map(formatRepositoryError)
    } yield {
      ReceiptPrinter.print(receipt, Some(data.paidAmount, change))
      change
    }
    result match {
      case Right(change) => Ok(change.toString)
      case Left(errString) => BadRequest(errString)
    }
  }

  private def checkStatusForClosing(r: Receipt) =
    Either.cond(r.isOpen, (), "receipt is not open")

  private def calculateChange(paid: BigDecimal, total: BigDecimal) = {
    val diff = paid - total
    if (diff.signum == -1) Left("insufficient payment")
    else Right(diff)
  }
  private def formatRepositoryError(err: ReceiptRepositoryError): String = err match {
    case ReceiptRepositoryError.DoesNotExist => "receipt does not exist"
    case ReceiptRepositoryError.UnknownError => "unknown error occured"
  }
}
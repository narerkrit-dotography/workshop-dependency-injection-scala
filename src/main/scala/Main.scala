package com.scale360
package workshop.di

import org.http4s.server.blaze._
import fs2._
import fs2.StreamApp.ExitCode

import cats.effect.IO

object Main extends StreamApp[IO] {

  val SERVER_PORT = 8080
  implicit val serveEC = scala.concurrent.ExecutionContext.Implicits.global

  override def stream(args: List[String], requestShutdown: IO[Unit]): Stream[IO, ExitCode] = {
    val serverStream = BlazeBuilder[IO]
      .bindLocal(SERVER_PORT)
      .withoutBanner
      .mountService(Api.routes)
      .serve

    def logErrorStream(t: Throwable) =
      Stream eval IO {
        println("error occurred in main app stream")
        println(t)
      }

    val exitOnSigStream = for {
      _ <- Stream eval IO(println("press ENTER to stop server"))
      _ <- Stream eval IO(scala.io.StdIn.readLine())
      _ <- Stream eval requestShutdown
    } yield ExitCode.Success

    val exitErrorStream = Stream eval requestShutdown map (_ => ExitCode.Error)
    serverStream merge exitOnSigStream handleErrorWith { t => logErrorStream(t) >> exitErrorStream }
  }
}
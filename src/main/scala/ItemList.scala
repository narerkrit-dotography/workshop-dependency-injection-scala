package com.scale360
package workshop.di

import Item._

object ItemList {
  private val rawRecords = Seq(
    "burger" -> 12.0,
    "spaghetti" -> 10.0,
    "salad" -> 6.5,
    "soup" -> 5.0,
    "fries" -> 2.9,
    "soda" -> 1.5,
    "juice" -> 1.5
  )

  val all: Seq[Item] = rawRecords.zipWithIndex
    .map{ case ((n, p), idx) =>
      Item(
        Id(idx + 1),
        Name(n),
        Price(BigDecimal valueOf p setScale 2))
    }
}
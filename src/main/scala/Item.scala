package com.scale360
package workshop.di

case class Item(id: Item.Id, name: Item.Name, price: Item.Price)

object Item{
  case class Id(value: Long)
  case class Name(value: String)
  case class Price(value: BigDecimal)
}
package com.scale360
package workshop.di

import scala.collection.mutable.{Map => MutMap}
import java.time.Instant

case class CreateReceiptData(openTime: Receipt.OpenTime)

object ReceiptRepository {
  private type Err = ReceiptRepositoryError
  private var state = MutMap.empty[Receipt.Id, Receipt]

  def create(createData: CreateReceiptData): Receipt.Id = {
    val newId = state.keys.toSeq
      .sortBy(_.value).lastOption
      .map(id => id.copy(value = id.value + 1L))
      .getOrElse(Receipt.Id(1L))
    val newReceipt = Receipt(newId, createData.openTime)
    state += (newId -> newReceipt)
    newId
  }

  def readAll(): Seq[Receipt] =
    state.values.toSeq.sortBy(_.id.value)

  def read(id: Receipt.Id): Either[Err, Receipt] =
    state.get(id).toRight(ReceiptRepositoryError.DoesNotExist)

  def update(id: Receipt.Id)(fn: Receipt => Receipt): Either[Err, Unit] = {
    state.get(id).map(fn) match {
      case Some(r) =>
        state.update(id,r)
        Right(())
      case None =>
        Left(ReceiptRepositoryError.DoesNotExist)
    }
  }

  def delete(id: Receipt.Id): Unit = state -= id
}

sealed trait ReceiptRepositoryError
object ReceiptRepositoryError {
  case object DoesNotExist extends ReceiptRepositoryError
  case object UnknownError extends ReceiptRepositoryError
}
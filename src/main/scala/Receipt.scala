package com.scale360
package workshop.di

import java.time.Instant

case class Receipt( id: Receipt.Id, items: Map[Item, Int], openTime: Receipt.OpenTime, status: Receipt.Status) {
  def addItem(i: Item, count: Int = 1): Receipt = addItemMap(Map(i -> count))

  def addItemMap(newItems: Map[Item, Int]): Receipt =
    copy(items = items ++ newItems.map{
      case (k,v) => k -> (v + items.getOrElse(k,0))
    })

  def isOpen = status match {
    case Receipt.Status.Open => true
    case Receipt.Status.Closed(_) => false
  }

  def calculateGrossTotal = items.foldLeft(BigDecimal(0)){
    case (sum, (item, count)) => sum + (item.price.value * count)
  }

  def setStatus(s: Receipt.Status) = copy(status = s)
}

object Receipt{
  case class Id(value: Long)
  case class OpenTime(value: Instant)
  case class CloseTime(value: Instant)

  sealed abstract class Status
  object Status {
    case object Open extends Status
    case class Closed(closedAt: CloseTime) extends Status
  }

  def apply(id: Id, openTime: OpenTime) = new Receipt(id, Map.empty, openTime, Status.Open)
}
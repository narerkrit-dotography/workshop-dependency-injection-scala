package com.scale360
package workshop.di

import io.circe._
import io.circe.generic.semiauto._

import org.http4s.circe._
import cats.effect.IO

case class OrderData(itemId: Item.Id, amount: Int)
case class CloseReceiptData(paidAmount: BigDecimal)

object RequestBodyCodec {
  // JSON
  private implicit val decodeItemId = Decoder[Long].map(Item.Id.apply)
  private implicit val decodeBigDecimal = Decoder[Double].map(BigDecimal.valueOf)

  private implicit val decodeOrderData = deriveDecoder[OrderData]
  private implicit val decodeCloseReceiptData = deriveDecoder[CloseReceiptData]

  // REQUEST BODY
  implicit val jsonOfSeqOrderData = jsonOf[IO, Seq[OrderData]]
  implicit val jsonOfCloseReceiptData = jsonOf[IO, CloseReceiptData]
}
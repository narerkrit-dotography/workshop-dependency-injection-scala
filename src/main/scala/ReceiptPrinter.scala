package com.scale360
package workshop.di


object ReceiptPrinter {
  def print(r: Receipt, paymentOpt: Option[(BigDecimal, BigDecimal)]): Unit = {
    println("**********************")
    println("FAKE PRINTER")
    println("**********************")
    println(s"receipt number: ${r.id}")
    r.items.foreach {
      case (item, amt) => println(
        s"  ${item.name.value} @ ${item.price.value} x ${amt} = ${item.price.value * amt}"
      )
    }
    val total = r.calculateGrossTotal
    println(s"total: $total")

    paymentOpt.foreach {
      case (paid, change) =>
        println(s"amount paid: $paid")
        println(s"change: $change")
    }
  }
}
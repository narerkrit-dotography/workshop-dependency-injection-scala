organization := "com.scale360"
name := "workshop-dependency-injection"
version := "1.0"

scalaVersion in ThisBuild := "2.12.8"

def Dep = Dependencies

lazy val service = (project in file("."))
  .settings(
    libraryDependencies ++=
      Dep.http4s ++
      Dep.circe ++
      Dep.macwire ++
      Dep.scalaTest )
# DEPENDENCY INJECTION IN SCALA
This code base is meant to be used as an exercise in the dependency injection workshop

## Instruction
You are to refactor the existing code to improve its quality, 
using the principles and techniques learned in the workshop.

Clone this repository to your account or download to your machine and work on that copy.
Do not make any changes to this repository directly.

## About the code
The code is a simple RESTful restaurant receipts and orders handling application.
It is intentionally left in low quality state, without proper use of dependency injection techniques.

The feature requirements are summarized as follows.

* Open new receipt
* Add items to the receipt
* Print receipt
* Make payment and close receipt

Since this is meant only as an exercise, 
these requirements are simplistic and do not stand up to real world requirements.
Feel free to try implementing any additional features as an exercise.

For simplicity, any required external system, e.g. database or printer, are replaced with mocked system instead.

## Tips for refactoring for dependency injection
- Identify separation of concerns, and break them into new classes.
- Recall SOLID principles
- start breaking from the top, and work down deeper one level at a time.
- create a new unit of code to tie all the components together just under the main class level.
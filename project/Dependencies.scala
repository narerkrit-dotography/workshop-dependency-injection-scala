import sbt._

object Versions {
  val http4s = "0.18.21"
  val circe = "0.9.3"
  val macwire = "2.3.3"
  val scalaTest = "3.0.1"
  val mockito = "2.8.47"
}

object Dependencies {
  private def V = Versions

  val http4s = Seq(
    "org.http4s" %% "http4s-dsl",
    "org.http4s" %% "http4s-blaze-server",
    "org.http4s" %% "http4s-circe",
  ) map (_ % V.http4s)

  val circe = Seq(
    "io.circe" %% "circe-core",
    "io.circe" %% "circe-generic"
  ) map (_ % V.circe)

  val macwire = Seq(
    "com.softwaremill.macwire" %% "macros" % V.macwire % "provided"
  )

  val scalaTest = Seq(
    "org.scalatest" %% "scalatest" % V.scalaTest,
    "org.mockito" % "mockito-core" % V.mockito,
  ) map (_ % Test)
}